<?php
  $titre = "ajout";
  $styleBody = "body";
  
  session_start();
  if( isset($_SESSION['username'])) 
  { 
?>  
  


<meta name="viewport" content="width=device-width, initial-scale=1"> <!--tag de bootsrap--> 
      
<html>
    <head>
        <meta charset="utf-8">
        <title>Esig'allais Manger !?</title>
        <meta name="viewport" content="width=device-width, initial-scale=1"> <!--tag de bootsrap--> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> <!--jquery-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"> <!--css de boostrap-->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script> <!--javascript de boostrap-->
        <link href="stylePlat.css" rel="stylesheet">
        <Title> Esig'allais manger !? </Title>

    </head>
    <body> 
        <header>
            
            <div class="wrapper">
                <h1><span class="jaune">Esig'allais manger !?</span></h1>
                
                <nav>
                    <ul>
                        <li>
                            <a href="pagePlat.php">Plats du jour</a>
                        </li>
                        <li> <a href="pageInfos.php">Infos</a></li>
                        <li><a href="tt_deconnexion.php">Déconnexion</a></li>
                        
                    </ul>
                </nav>

            </div>
</div>
        </header>

<div class="container">
  <div class="row"> 
    <div class="col-md-3 col-sm-3 col-lg-3 offset-md-4"style="color: #fff;">
    <div class="ccly">

      <h1>Ajouter les plats</h1>

      <form class="row g-3" action="tt_ajout_plat.php" method="post">
        <p>le nom du plat:<input type="text" required name="platnom" /></p>
        <p>l'image du plat:<input type="text" required name="platimage" /></p>
        <p style="text-align:left">le categorie du plat:<select style="direction:ltr" name="platcat">
          <option value="entree">Entree</option>
          <option value="plat">plat</option>
          <option value="boisson">boisson</option>
          <option value="dessert">dessert</option>
        </select></p>
        <p>la description du plat:<input type="text" required name="platdescrip" /></p>
        <p>le prix du plat:<input type="text" required name="platprix" /></p>
        <p><input type="submit" name="ajoutsub" value="ajouter" /></p>
     </div>
</div>
  </div>

   
    </div>

    <footer>
            <div class="wrapper">
            <a href='acceuil_respo.php' class='button-5'>Retour</a>
            </div>
                     <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
            <img src="https://www.nae.fr/wp-content/uploads/2021/10/ESIGELEC-Rouen-3.png" width="300" height="100">
         </footer>


    <?php }
        else 
            header('Location : pageConnexion.php')

?>
