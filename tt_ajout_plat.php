<?php
  session_start(); // Pour les massages

  
  // Contenu du formulaire :
  $platnom =  htmlentities($_POST['platnom']);
  $platprix =  htmlentities($_POST['platprix']);
  $platcat = htmlentities($_POST['platcat']);
  $platdescrip =  htmlspecialchars($_POST['platdescrip']);
  $platimage =  htmlspecialchars($_POST['platimage']);


  // Connexion :
  require_once("param.inc.php");
  $mysqli = new mysqli($host, $name, $passwd, $dbname);
  if ($mysqli->connect_error) {
      die('Erreur de connexion (' . $mysqli->connect_errno . ') '
              . $mysqli->connect_error);
  }

  if ($stmt = $mysqli->prepare("INSERT INTO plat(nom, prix_plat, categorie, description, photo) VALUES ( ?, ?, ?, ? ,?)")) {
    $stmt->bind_param("sisss" ,$platnom, $platprix, $platcat, $platdescrip, $platimage);
    if($stmt->execute()) {
        $_SESSION['success'] = "Enregistrement réussi"; 
        header('Location: acceuil_respo.php');
    } else {
        $_SESSION['success'] =  "Impossible d'enregistrer";
        header('Location: ajoutplat.php');
    }
  }



?>