<?php
?>

<html>
    <head>
        <meta charset="utf-8"> <!--pour que les accents soient pris en compte-->
        <link href="styleConnexion.css" rel="stylesheet">
        <title>Connexion</title>
    </head>

    <body>
        <header>
            <div class="wrapper">
                <h3>CONNEXION</h3>
                <p> Veuillez renseigner votre identifiant et mot de passe</p>
                <form action="tt_connexion.php" method="post">
                    <label for="email">Email</label>
                    <input type="text" id="email" name="l_email" placeholder="Votre email">
                    <br>
                    <label for="mdp">Mot De Passe</label>
                    <input type="password" id="mdp" name="le_pass" placeholder="Votre mot de passe">

                    <input type="submit" value="OK" class="button-6">
                </form>
            </div>
        </header>
            
        <footer>
                
            <div class="wrapper">
                    <a href="index.php" class="button-7">Retour</a>
            </div>
            <img src="https://www.nae.fr/wp-content/uploads/2021/10/ESIGELEC-Rouen-3.png" width="300" height="100">


        </footer>
    </body>
</html>