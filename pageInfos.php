<?php
?>

<html>
    <head>
        <meta charset="utf-8">
        <link href="styleInfo.css" rel="stylesheet">
        <title>Infos</title>
    </head>
    <body>
         <header>
            <div class="wrapper"> 
                
                    <h1>Esig'allais manger !?</h1>
                   
            </div>
         </header>
        <section id="informations">
            <div class="wrapper">
                <h2><strong>OUVERT - FERME À 18H00</strong></h2>
                <br>
                <br>
                <br>
                <h3><strong>INFOS</strong></h3>
                <h4>Esig'allais manger <br> Technopôle du Madrillet, Av. Galilée, 76800 Saint-Étienne-du-Rouvray<br>FRANCE <br> 02 09 25 88 70</h4>
                <br>
                <br>
                <br>
                <h3><strong>HORAIRES</strong></h3>
                <h4>Lundi 12H00 - 18H00 <br> Mardi 12H00 - 18H00 <br> Mercredi 12H00 - 18H00 <br> Jeudi 12H00 - 18H00 <br> Vendredi 12H00 - 18H30</h4>
            </div>
        </section>
        <footer>
            <div class="wrapper">

                <?php 
                    session_start();
                    if ((isset($_SESSION['username'])) && $_SESSION['type_user']==0)
                    {
                        echo "<a href='acceuil_etudiant.php' class='button-5'>Retour</a>";
                    } 
                    else if((isset($_SESSION['username'])) && $_SESSION['type_user']==1)
                    {
                        echo "<a href='acceuil_respo.php' class='button-5'>Retour</a>";
                    }
                    else
                    {
                        echo "<a href='index.php' class='button-5'>Retour</a>";
                    }
                    ?>
            </div>
            <img src="https://www.nae.fr/wp-content/uploads/2021/10/ESIGELEC-Rouen-3.png" width="300" height="100">


        </footer>