<?php
  session_start(); // Pour les massages

  
  // Contenu du formulaire :
  $email =  htmlentities($_POST['l_email']);
  $password = htmlentities($_POST['le_pass']);
  
  // Option pour bcrypt
  $options = [
        'cost' => 12,
  ];

  // Connexion :
  require_once("param.inc.php");
  $mysqli = new mysqli($host, $name, $passwd, $dbname);
  if ($mysqli->connect_error) {
      die('Erreur de connexion (' . $mysqli->connect_errno . ') '
              . $mysqli->connect_error);
  }


  if ($stmt = $mysqli->prepare("SELECT * FROM `utilisateur` where email=?;
")) {
    $stmt->bind_param("s", $email);
    // Le message est mis dans la session, il est préférable de séparer message normal et message d'erreur.
    if($stmt->execute()) {
		/* instead of bind_result: */
		$result = $stmt->get_result();

		/* now you can fetch the results into an array - NICE */
		while ($myrow = $result->fetch_assoc()) {

			// use your $myrow array as you would with any other fetch
			print_r($myrow);
			echo $password;
			if(password_verify($password,$myrow['mdp'])){
				echo "<h1> Connexion reussi </h1>";
				$_SESSION['username'] = $myrow['email'];
				$_SESSION['type_user'] = $myrow['type_user'];
        $_SESSION['name'] = $myrow['prenom'];
				
				$_SESSION['message'] = "Connection réussi";
				if($myrow['type_user']==0)
					header('Location: connexion_respo.php');
          

				else if ($myrow['type_user']==1)
					header('Location: connexion_respo.php');

				exit();
			}else {
				$_SESSION['message'] =  "Impossible de se connecter";
				header('Location: pageConnexion.php');
				exit();

			}

		}
		//header('Location: connexion.php');
    } else {
		//header('Location: connexion.php');
    }
  }
  // Redirection vers la page d'accueil 
  // Où le message présent dans la session sera affiché.

?>

