<?php
?>

<html>
    <head>
        <meta charset="utf-8">
        <link href="styleCreation.css" rel="stylesheet">
        <title>Créer compte</title>
    </head>

    <body>
        <header>
            <div class="wrapper">
                <h3>CREER UN COMPTE</h3>
                <p>Si vous n'avez pas encore de compte, n'hésitez pas à en créer un! <br> Remplissez vos informations personnelles.</p>

                <form action = "tt_creation.php" method="post">
                    <label for="le_nom">Nom</label>
                    <input type="text" id="nom" placeholder="Votre nom" required name="le_nom">
                    <div class="col-md-6">
                        <label for="le_prenom">Prénom</label>
                        <input type="text" id="prenom" placeholder="Votre prénom" required name="le_prenom">
                    </div>
                    <div class="col-md-6">
                        <label for="l_email">Email</label>
                        <input type="text" id="email" placeholder="Votre email" required name="l_email">
                    </div>
                    <div class="col-md-6">
                        <label for="le_mdp">Mot De Passe</label>
                        <input type="password" id="mdp" placeholder="Votre mot de passe" required name="le_pass">
                    </div>
                    
                    <div class="row my-3">
                    <div class="d-grid gap-2 d-md-block">
                    <input type="submit" value="Envoyer" class="button-5">
                    </div>
                    </div>
                    </form>
            </div>
        </header>
        <footer>
            
                <div class="wrapper">
                    <a href="index.php" class="button-4">Retour</a>
                </div>
        
                <img src="https://www.nae.fr/wp-content/uploads/2021/10/ESIGELEC-Rouen-3.png" width="300" height="100">

        </footer>
    </body>
</html>
